export type UserCompanyType = {
  name?: string | null,
  catchPhrase?: string | null,
  bs?: string | null
}

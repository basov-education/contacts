import { UserAddressType } from '@/type/UserAddressType'
import { UserCompanyType } from '@/type/UserCompanyType'

export type UserType = {
    id?: number | string | null,
    name?: string | null,
    username?: string | null,
    email?: string | null,
    address?: UserAddressType | null,
    phone?: string | null,
    website?: string | null,
    company?: UserCompanyType | null
}

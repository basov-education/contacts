export type UserAddressType = {
    street?: string | null,
    suite?: string | null,
    city?: string | null,
    zipcode?: string | null,
    geo?: {
      lat?: string | number | null,
      lng?: string | number | null,
    }
}

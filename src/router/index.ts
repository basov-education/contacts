import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import UserListView from '@/views/UserListView.vue'
import UserView from '@/views/UserView.vue'
import UserCreateView from '@/views/UserCreateView.vue'

const routes: Array<RouteRecordRaw> = [
  // Список пользователей
  {
    path: '/',
    name: 'UserList',
    component: UserListView
  },
  // Детальная страница пользователя
  {
    path: '/user/:id',
    name: 'User',
    component: UserView
  },
  // Создание пользователя
  {
    path: '/user/create',
    name: 'UserCreate',
    component: UserCreateView
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: UserListView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router

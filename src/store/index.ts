import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {},
  getters: {},
  actions: {
    getUserList ({ commit }) {
      return new Promise((resolve, reject) => {
        axios({
          url: 'https://jsonplaceholder.typicode.com/users',
          method: 'GET'
        })
          .then((resp) => {
            resolve(resp?.data)
          })
          .catch((err) => {
            console.warn(err)
            reject(err)
          })
      })
    },

    getUserById ({ commit }, data) {
      return new Promise((resolve, reject) => {
        axios({
          url: `https://jsonplaceholder.typicode.com/users/${data.id}`,
          method: 'GET'
        })
          .then((resp) => {
            resolve(resp?.data)
          })
          .catch((err) => {
            console.warn(err)
            reject(err)
          })
      })
    },

    createUser ({ commit }, data) {
      return new Promise((resolve, reject) => {
        axios({
          url: 'https://jsonplaceholder.typicode.com/users',
          method: 'POST',
          data: data
        })
          .then((resp) => {
            resolve(resp?.data)
          })
          .catch((err) => {
            console.warn(err)
            reject(err)
          })
      })
    }
  },
  mutations: {},
  modules: {}
})
